with Text_IO;
with Ada.Text_IO;
with Ada.Integer_Text_IO;
with Interfaces.C;

use Ada.Integer_Text_IO;
use Ada.Text_IO;
use Text_IO;
use Interfaces.C;

package body Screen is

procedure Clear is
  function  System(Arg : Char_Array) return Integer;
  pragma    Import(C, System, "system");
  Ret_Val : Integer;
begin
  Ret_Val := System(To_C("cls || clear "));   -- "cls" for windows, "clear" for linux.
end Clear;


procedure Draw is
  i,j : Integer := 0;
begin

 for i in 1 .. 20 loop

   for j in 1 .. 79 loop
     Put("#");
   end loop;

   Put_Line("");
 end loop;

end Draw;



end Screen;
