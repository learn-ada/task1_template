with Text_IO;
with Ada.Text_IO;
with Ada.Integer_Text_IO;
with Screen;

use Ada.Integer_Text_IO;
use Ada.Text_IO;
use Text_IO;

procedure Task1 is
  counter      : integer := 0;
  input_symbol : Character := 'F';
begin

  Put_Line("Start");

  Put("counter = ");
  Put_Line(integer'Image(counter));   -- comments

  for counter in 1 .. 10 loop
    Get(input_symbol);
    Screen.Clear;
    Screen.Draw;
  end loop;

  Put_Line("Exit.");

end Task1;

